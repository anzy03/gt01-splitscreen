# SplitScreen Platformer

This game is a 2D Slip Screen Couch Versus Game. Where u playy against someone else and compete to finish the game first.

### How to play.
- Player 1 uses A & D to move, W to jump.
- Player 2 uses Left & Right Arrows to move and Up Arrows to jump.

- Click on start start playing.

### Assets used from.

Pixel Adventure - [itch.io](https://pixelfrog-store.itch.io/pixel-adventure-1).

### Dependances.
[Cinemachine](https://unity.com/unity/features/editor/art-and-design/cinemachine).
